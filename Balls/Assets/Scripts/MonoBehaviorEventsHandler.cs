﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MonoBehaviorEventsHandler : MonoBehaviour {

    [SerializeField]
    private UnityEvent OnStartEvent;

    [SerializeField]
    private UnityEvent OnUpdateEvent;

    [SerializeField]
    private UnityEvent OnEnableEvent;

    [SerializeField]
    private UnityEvent OnDisableEvent;

    [SerializeField]
    private UnityEvent OnMouseDownEvent;

    // Use this for initialization
    void Start () {
        OnStartEvent.Invoke();
    }
	
	// Update is called once per frame
	void Update () {
        OnUpdateEvent.Invoke();
    }

    void OnEnable()
    {
        OnEnableEvent.Invoke();
    }

    void OnDisable()
    {
        OnDisableEvent.Invoke();
    }

    private void OnMouseDown()
    {
        OnMouseDownEvent.Invoke();
    }
}
