﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

    [SerializeField]
    public float Area = 1;

    [SerializeField]
    public Rigidbody2D RigidBody;

    [SerializeField]
    private Renderer Renderer;

    private float SettedArea = 0;

    public bool IsActiveAbsorption = false;

    public IColorByAreaGetter ColorByAreaGetter;

    public GameEndObserver GameEndObserver;

    public bool IsDestroy;

    // Use this for initialization
    void Start () {
        IsDestroy = false;

    }
	
	// Update is called once per frame
	void Update () {
        
        if(SettedArea!= Area)
        {
            SettedArea = Area;
            float size = Mathf.Sqrt(4 * SettedArea / Mathf.PI);
            gameObject.transform.localScale = new Vector3(size, size, 1);
            Renderer.material.color = ColorByAreaGetter.GetColorByArea(SettedArea);
        }
        
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!IsActiveAbsorption)
        {
            return;
        }
        Ball ball = collision.gameObject.GetComponent<Ball>();

        if (ball != null)
        {
            Ball bigBall;
            Ball smallBall;
            if(ball.Area > Area)
            {
                bigBall = ball;
                smallBall = this;
            }
            else
            {
                bigBall = this;
                smallBall = ball;
            }

            bigBall.Area += smallBall.Area;
            GameEndObserver.BallChangeSize(bigBall);

            smallBall.Area = 0;
            Destroy(smallBall.gameObject);
        }
    }

    public void Sleep()
    {
        if (IsDestroy)
        {
            return;
        }
        RigidBody.Sleep();
    }

    public void AddForce(Vector2 force)
    {
        if(IsDestroy)
        {
            return;
        }
        RigidBody.AddForce(force);
    }

    private void OnDestroy()
    {
        IsDestroy = true;
    }


}
