﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.Events;

public class LoadingController : MonoBehaviour {

    [SerializeField]
    private GameModel GameModel;

    [SerializeField]
    private UnityEvent LoadingCompleteEvent;

    private string ConfigJsonFileName = "config.json";

	// Use this for initialization
	void Start () {
        LoadConfigFile();
    }
	
    private void LoadConfigFile()
    {
        string configFilePath = Path.Combine(Application.streamingAssetsPath, ConfigJsonFileName);
        if (File.Exists(configFilePath))
        {
            string configDataString = File.ReadAllText(configFilePath);
            GameSettings gameSettings = JsonUtility.FromJson<GameSettings>(configDataString);
            GameModel.GameSettings = gameSettings;
        }

        StartCoroutine(LoadingComplete());

    }

    private IEnumerator LoadingComplete()
    {
        yield return new WaitForSeconds(1);
        LoadingCompleteEvent.Invoke();
    }

	// Update is called once per frame
	void Update () {
		
	}
}
