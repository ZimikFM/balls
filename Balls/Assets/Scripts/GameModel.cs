﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameModel : MonoBehaviour  {

    public List<Ball> Balls = new List<Ball>();

    public Ball PlayersBall;

    public float TotalArea;

    public GameSettings GameSettings;

    

    public void StartAbsorbtion()
    {
        for (int i = 0; i< Balls.Count; i++)
        {
            Balls[i].IsActiveAbsorption = true;
        }
    }

    public void SleepAllBalls()
    {
        for (int i = 0; i < Balls.Count; i++)
        {
            Balls[i].Sleep();
        }
    }

}
