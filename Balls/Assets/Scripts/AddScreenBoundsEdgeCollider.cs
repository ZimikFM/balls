﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddScreenBoundsEdgeCollider : MonoBehaviour {

    [SerializeField]
    private Camera Camera;

    [SerializeField]
    private EdgeCollider2D EdgeCollider;



    // Use this for initialization
    void Start () {
		if(Camera == null)
        {
            Debug.LogError("Camera don't set");
            return;
        }

        if (!Camera.orthographic)
        {
            Debug.LogError("Camera is not orthographic");
            return;
        }

        if(EdgeCollider == null)
        {
            EdgeCollider = gameObject.AddComponent<EdgeCollider2D>();
        }

        Vector2 bottomLeft = Camera.ScreenToWorldPoint(new Vector3(0,0,Camera.nearClipPlane));
        Vector2 topLeft = Camera.ScreenToWorldPoint(new Vector3(0, Camera.pixelHeight, Camera.nearClipPlane));
        Vector2 topRight = Camera.ScreenToWorldPoint(new Vector3(Camera.pixelWidth, Camera.pixelHeight, Camera.nearClipPlane));
        Vector2 bottomRight = Camera.ScreenToWorldPoint(new Vector3(Camera.pixelWidth, 0, Camera.nearClipPlane));

        Vector2[] edgePoints= new Vector2[] {bottomLeft, topLeft, topRight, bottomRight, bottomLeft};

        EdgeCollider.points = edgePoints;

    }

    // Update is called once per frame
    void Update () {
		
	}
}
