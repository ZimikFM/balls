﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameSettings {

    public int enemiesCount;
    public BallsSettings user;
    public BallsSettings enemy;

}

[System.Serializable]
public class BallsSettings
{
    public Color minAreaColor;
    public Color maxAreaColor;
}
