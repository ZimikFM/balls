﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameEndScreenController : MonoBehaviour {

    [SerializeField]
    private GameModel GameModel;

    [SerializeField]
    private UnityEvent ShowPlayerWinInfoEvent;

    [SerializeField]
    private UnityEvent ShowPlayerLooseInfoEvent;


    // Use this for initialization
    void Start () {
		
	}

    void OnEnable()
    {
        if(GameModel.TotalArea/2< GameModel.PlayersBall.Area)
        {
            ShowPlayerWinInfoEvent.Invoke();
        }
        else
        {
            ShowPlayerLooseInfoEvent.Invoke();
        }
    }
}
