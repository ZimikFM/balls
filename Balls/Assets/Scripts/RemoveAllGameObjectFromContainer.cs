﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveAllGameObjectFromContainer : MonoBehaviour {

    [SerializeField]
    private GameObject Container;

    public void RemoveAll()
    {
        if (Container != null)
        {
            for(int i = 0; i < Container.transform.childCount; i++)
            {
                Destroy(Container.transform.GetChild(i).gameObject);
            }
        }
    }
}
