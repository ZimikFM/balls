﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameEndObserver : MonoBehaviour {

    public float TotalArea;

    [SerializeField]
    private UnityEvent GameCompleteEvent;

	public void BallChangeSize(Ball ball)
    {
        if (TotalArea / 2 < ball.Area)
        {
            GameCompleteEvent.Invoke();
        }
    }
}
