﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickController : MonoBehaviour {

    [SerializeField]
    private GameModel GameModel;


    private void OnMouseDown()
    {
        if (GameModel.PlayersBall.IsDestroy)
        {
            return;
        }
        Vector3 mousePosition =  Input.mousePosition;
        Vector3 objectPosition = Camera.main.WorldToScreenPoint(GameModel.PlayersBall.transform.position);
        Vector2 forceVector = objectPosition - mousePosition;
        GameModel.PlayersBall.AddForce(forceVector);
    }
}
