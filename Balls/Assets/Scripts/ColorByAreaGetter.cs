﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorByAreaGetter : MonoBehaviour, IColorByAreaGetter
{

    public Color MinAreaColor;

    public Color MaxAreaColor;

    public float TotalArea;

    public Color GetColorByArea(float Area)
    {
        float halfArea = TotalArea;
        return (MaxAreaColor - MinAreaColor) * (Area / halfArea) + MinAreaColor;

    }
}
