﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetWidthByScreen : MonoBehaviour {

	// Use this for initialization
	void Start () {
        gameObject.transform.localScale = new Vector3((float)Screen.width / (float)Screen.height, 1, 1);
    }
	
}
