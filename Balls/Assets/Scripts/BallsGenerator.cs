﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BallsGenerator : MonoBehaviour {

    [SerializeField]    
    private float MinBallArea;

    [SerializeField]
    private float MaxBallArea;

    [SerializeField]
    private int BallsCount;

    [SerializeField]
    private float StartForce;

    [SerializeField]
    private GameObject Container;

    [SerializeField]
    private GameObject BallsTemplate;

    [SerializeField]
    private GameObject PlayerTemplate;

    [SerializeField]
    private GameModel GameModel;

    [SerializeField]
    private ColorByAreaGetter BallsColorGetter;

    [SerializeField]
    private ColorByAreaGetter PlayersColorGetter;

    [SerializeField]
    private GameEndObserver GameEndObserver;

    [SerializeField]
    private UnityEvent GenerateBallsCompleteEvent;

    private void Start()
    {
        
    }

    public void GenerateBalls()
    {

        GameModel.TotalArea = 0;
        GameModel.Balls = new List<Ball>();

        for ( int i = 0; i< BallsCount; i++)
        {
            Ball ballObject = Instantiate(i==0? PlayerTemplate:BallsTemplate, Container.transform).GetComponent<Ball>();

            ballObject.transform.localPosition = new Vector3(Random.Range(-4f,4f), Random.Range(-4f, 4f),0);
            ballObject.Area = Random.Range(MinBallArea, MaxBallArea);
            ballObject.ColorByAreaGetter = BallsColorGetter;
            GameModel.TotalArea += ballObject.Area;
            GameModel.Balls.Add(ballObject);
            ballObject.AddForce(new Vector2(Random.Range(0, StartForce), Random.Range(0, StartForce)));
            ballObject.GameEndObserver = GameEndObserver;
        }

        GameEndObserver.TotalArea = BallsColorGetter.TotalArea = PlayersColorGetter.TotalArea = GameModel.TotalArea;

        GameModel.PlayersBall = GameModel.Balls[0];
        GameModel.PlayersBall.ColorByAreaGetter = PlayersColorGetter;


        StartCoroutine(StartAbsorbtion());
    }



    private IEnumerator StartAbsorbtion()
    {
        yield return new WaitForSeconds(1);

        GenerateBallsCompleteEvent.Invoke();
       

    }

}
