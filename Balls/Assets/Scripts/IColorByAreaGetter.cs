﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IColorByAreaGetter  {

    Color GetColorByArea(float Area);
}
